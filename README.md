# rc_script

## Table of Contents

1. [Description](#description)
1. [Usage](#usage)
1. [Reference](#reference)
1. [Limitations](#limitations)
1. [Development](#development)

## Description

This module helps create BSD style [rc.d](https://man.freebsd.org/rc.d) scripts.

## Usage

  Here's an example of creating an rc script for [The Lounge](https://thelounge.chat/), a NodeJS based IRC client that's ran as Web Server:

```puppet
rc_script { 'thelounge':
  ensure        => 'present',
  user          => 'thelounge',
  group         => 'thelounge',
  cwd           => '/srv/thelounge',
  env           => { 'THELOUNGE_HOME' => '/srv/thelounge'},
  daemonize     => true,
  use_syslog    => true,
  restart_delay => 30, # seconds,
  command       => '/usr/local/bin/thelounge',
  command_args  => 'start',
}
```

This will create a file in `/usr/local/etc/rc.d/thelounge`:

```sh
#!/bin/sh

# PROVIDE: thelounge
# REQUIRE: DAEMON
# KEYWORD: shutdown

#
# Add the following lines to /etc/rc.conf to enable thelounge
#
# thelounge_enable="YES"

. /etc/rc.subr

name=thelounge
rcvar=thelounge_enable
desc="thelounge daemon"

load_rc_config $name

: ${thelounge_enable:=NO}
thelounge_user=thelounge
thelounge_group=thelounge
thelounge_chdir=/srv/thelounge
thelounge_env=THELOUNGE_HOME=/srv/thelounge

procname=/usr/local/bin/thelounge
command=/usr/sbin/daemon
command_args="-T thelounge -t ${name} -R 30 ${procname} start"

run_rc_command "$1"
```

## Reference

See [REFERENCE.md](REFERENCE.md)

## Limitations

As a first shot, we'll be supporting FreeBSD and FreeBSD derived Operating Systems, such as Dragonfly and HardenedBSD.
More is to follow, as OpenBSD and NetBSD both use `rc.d`, but have different sets of supported options / capabilities.
See their respective `rc.subr` man pages for the differences:

- [FreeBSD's rc.subr](https://man.freebsd.org/cgi/rc.subr)
- [NetBSD's rc.subr](https://man.netbsd.org/rc.subr.8)
- [OpenBSD's rc.subr](https://man.openbsd.org/rc.subr.8)


## Development

This module uses [pdk](https://puppet.com/docs/pdk/latest/pdk_generating_modules.html) for development and testing of the module.
Contributions are very welcome in the form of issues and Pull Requests.
