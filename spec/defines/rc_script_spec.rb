# frozen_string_literal: true

require 'spec_helper'

describe 'rc_script' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'please just compile' do
        let(:title) { 'namevar' }
        let(:params) do
          { command: '/bin/true' }
        end

        it { is_expected.to compile.with_all_deps }
        it do
          is_expected.to contain_file(title)
            .with_content(%r{command="/bin/true"})
        end
      end

      context 'readme example' do
        let(:title) { 'thelounge' }
        let(:name) { 'thelounge' }
        let(:params) do
          {
            ensure: 'present',
            user: 'thelounge',
            group: 'thelounge',
            cwd: '/srv/thelounge',
            env: { 'THELOUNGE_HOME' => '/srv/thelounge' },
            daemonize: true,
            use_syslog: true,
            restart_delay: 30, # seconds,
            command: '/usr/local/bin/thelounge',
            command_args: 'start',
          }
        end

        it { is_expected.to compile.with_all_deps }
        it do
          is_expected.to contain_file(title)
            .with_path('/usr/local/etc/rc.d/thelounge')
            .with_mode('0755')
            .with_owner('root')
            .with_group('wheel')
        end

        it do
          is_expected.to contain_file(title)
            .with_content(%r{# PROVIDE: thelounge})
            .with_content(%r{name=thelounge})
            .with_content(%r{desc="thelounge daemon"})
            .with_content(%r{thelounge_user=thelounge})
            .with_content(%r{thelounge_group=thelounge})
            .with_content(%r{thelounge_chdir=/srv/thelounge})
            .with_content(%r{thelounge_env=THELOUNGE_HOME=/srv/thelounge})
            .with_content(%r{command=/usr/sbin/daemon})
            .with_content(%r{command_args="-T #{name} -t \$\{name\} -R 30 \$\{procname\} start"})
        end
      end
    end
  end
end
